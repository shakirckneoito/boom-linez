
resource "aws_api_gateway_rest_api" "main" {
  name        = var.GatewayName
  description = var.GatewayDescription
}
resource "aws_api_gateway_resource" "main" {
  rest_api_id = aws_api_gateway_rest_api.main.id
  parent_id   = aws_api_gateway_rest_api.main.root_resource_id
  path_part   = "testpath"
  depends_on = [
    aws_api_gateway_rest_api.main
  ]
}
resource "aws_api_gateway_deployment" "main" {
  rest_api_id = aws_api_gateway_rest_api.main.id
  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.main,
      aws_api_gateway_method.main,
      aws_api_gateway_integration.main,
      aws_api_gateway_method_response.main,
      # aws_api_gateway_usage_plan.main,
    ]))
  }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_api_gateway_stage" "main" {
  deployment_id = aws_api_gateway_deployment.main.id
  rest_api_id   = aws_api_gateway_rest_api.main.id
  stage_name    = var.StageName
}

resource "aws_api_gateway_usage_plan" "main" {
  name        = var.UsagePlanName
  description = var.UsagePlanDescription
  api_stages {
    api_id = aws_api_gateway_rest_api.main.id
    stage  = aws_api_gateway_stage.main.stage_name
  }
  quota_settings {
    limit  = var.QuotaLimit
    offset = var.QuotaOffset
    period = var.QuotaPeriod
  }
  throttle_settings {
    burst_limit = var.BurstLimit
    rate_limit  = var.RateLimit
  }
}
resource "aws_api_gateway_api_key" "main" {
  count       = var.APIKeyCount
  name        = "${var.APIKeyNamePrefix}-${count.index}"
  description = var.APIKeyDescription
  enabled     = true
}
resource "aws_api_gateway_usage_plan_key" "main" {
  count         = var.APIKeyCount
  key_id        = aws_api_gateway_api_key.main[count.index].id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.main.id
}
resource "aws_api_gateway_method" "main" {
  rest_api_id      = aws_api_gateway_rest_api.main.id
  resource_id      = aws_api_gateway_resource.main.id
  http_method      = "GET"
  authorization    = "NONE"
  api_key_required = true
  depends_on = [
    aws_api_gateway_resource.main,
    aws_api_gateway_rest_api.main
  ]
}
resource "aws_api_gateway_integration" "main" {
  rest_api_id             = aws_api_gateway_rest_api.main.id
  resource_id             = aws_api_gateway_resource.main.id
  http_method             = aws_api_gateway_method.main.http_method
  type                    = "HTTP"
  integration_http_method = "GET"
  uri                     = "http://${aws_lb.alb.dns_name}/accounts"
  depends_on = [
    aws_api_gateway_method.main,
    aws_api_gateway_rest_api.main,
    aws_api_gateway_resource.main
  ]
}
resource "aws_api_gateway_integration_response" "main" {
  rest_api_id = aws_api_gateway_rest_api.main.id
  resource_id = aws_api_gateway_resource.main.id
  http_method = aws_api_gateway_integration.main.http_method
  status_code = "200"
  depends_on = [
    aws_api_gateway_integration.main,
    aws_api_gateway_rest_api.main,
    aws_api_gateway_resource.main
  ]
}
resource "aws_api_gateway_method_response" "main" {
  rest_api_id = aws_api_gateway_rest_api.main.id
  resource_id = aws_api_gateway_resource.main.id
  http_method = aws_api_gateway_integration.main.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  depends_on = [
    aws_api_gateway_integration_response.main,
    aws_api_gateway_integration.main,
  ]
}
