resource "aws_route_table" "public_rt" {
  vpc_id = aws_vpc.vpc.id
  tags   = var.PublicRouteTableTags
  depends_on = [
    aws_vpc.vpc
  ]
}
resource "aws_route" "public" {
  route_table_id         = aws_route_table.public_rt.id
  destination_cidr_block = var.PublicRouteTableCIDR
  gateway_id             = aws_internet_gateway.igw.id
  depends_on = [
    aws_route_table.public_rt, aws_internet_gateway.igw
  ]
}
resource "aws_route_table" "private_rt" {
  count  = var.SubnetCount
  vpc_id = aws_vpc.vpc.id
  tags   = var.PrivateRouteTableTags
  depends_on = [
    aws_vpc.vpc
  ]
}
resource "aws_route" "private" {
  count                  = var.SubnetCount
  route_table_id         = aws_route_table.private_rt[count.index].id
  destination_cidr_block = var.PrivateRouteTableCIDR
  nat_gateway_id         = aws_nat_gateway.main_nat[count.index].id
  depends_on = [
    aws_route_table.private_rt, aws_nat_gateway.main_nat
  ]
}
resource "aws_route_table_association" "public_rta" {
  count          = var.SubnetCount
  subnet_id      = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.public_rt.id
  depends_on = [
    aws_route_table.public_rt, aws_subnet.public_subnets
  ]
}
resource "aws_route_table_association" "private_rta" {
  count          = var.SubnetCount
  subnet_id      = aws_subnet.private_subnets[count.index].id
  route_table_id = aws_route_table.private_rt[count.index].id
  depends_on = [
    aws_route_table.private_rt, aws_subnet.private_subnets
  ]
}

