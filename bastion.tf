resource "aws_instance" "bastion" {
  count                       = var.EnableBastion ? var.BastionCount : 0
  ami                         = data.aws_ami.amazon_linux2.id
  subnet_id                   = aws_subnet.public_subnets[count.index % var.SubnetCount].id
  associate_public_ip_address = true
  key_name                    = var.KeyPairName
  vpc_security_group_ids      = [aws_security_group.bastion.id]
  instance_type               = var.BastionInstanceType
  tags                        = var.BastionInstanceTags
  depends_on                  = [aws_internet_gateway.igw, aws_subnet.public_subnets]
}

