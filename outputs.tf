output "alb_dns" {
  value       = aws_lb.alb.dns_name
  description = "DNS name of the ALB"
}

output "cloudfront" {
  value       = aws_api_gateway_domain_name.linez.cloudfront_domain_name
  description = "CloudFront distribution"
}

output "custom_domain" {
  value       = aws_api_gateway_domain_name.linez.domain_name
  description = "Custom domain name"
}
