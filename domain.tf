data "aws_acm_certificate" "linez" {
  domain   = var.AcmCertDomain
  statuses = var.AcmCertStatuses
}
data "aws_route53_zone" "selected" {
  name = var.HostedZoneName
}
resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.Route53RecordName
  type    = var.Route53RecordType
  alias {
    evaluate_target_health = true
    name                   = aws_api_gateway_domain_name.linez.cloudfront_domain_name
    zone_id                = aws_api_gateway_domain_name.linez.cloudfront_zone_id
  }
  depends_on = [aws_api_gateway_domain_name.linez]
}
resource "aws_api_gateway_domain_name" "linez" {
  certificate_arn = data.aws_acm_certificate.linez.arn
  domain_name     = var.AcmCertDomain
  depends_on = [
    data.aws_acm_certificate.linez
  ]
}
resource "aws_api_gateway_base_path_mapping" "main" {
  api_id      = aws_api_gateway_rest_api.main.id
  stage_name  = aws_api_gateway_stage.main.stage_name
  domain_name = aws_api_gateway_domain_name.linez.domain_name
  depends_on = [
    aws_api_gateway_rest_api.main,
    aws_api_gateway_stage.main,
    aws_api_gateway_domain_name.linez
  ]
}
