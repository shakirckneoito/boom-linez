resource "aws_subnet" "public_subnets" {
  vpc_id                          = aws_vpc.vpc.id
  count                           = var.SubnetCount
  cidr_block                      = cidrsubnet(aws_vpc.vpc.cidr_block, 4, count.index)
  availability_zone               = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch         = var.PublicSubnetMapPublicIpOnLaunch
  assign_ipv6_address_on_creation = var.PublicSubnetAssignIpv6AddressOnCreation
  tags                            = var.PublicSubnetTags
  depends_on                      = [aws_internet_gateway.igw]
}
resource "aws_subnet" "private_subnets" {
  vpc_id            = aws_vpc.vpc.id
  count             = var.SubnetCount
  cidr_block        = cidrsubnet(aws_vpc.vpc.cidr_block, 4, count.index + var.SubnetCount)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags              = var.PrivateSubnetTags
  depends_on        = [aws_internet_gateway.igw]
}
