provider "aws" {
  region = var.awsRegion
}
data "aws_availability_zones" "available" {
  state = "available"
}
