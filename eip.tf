resource "aws_eip" "nat_eip" {
  count = var.SubnetCount
  vpc   = true
  depends_on = [
    aws_internet_gateway.igw
  ]
}
