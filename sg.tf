
# bastion egress


resource "aws_security_group" "bastion" {
  # name        = "terraform-remote-bastion"
  name_prefix = var.BastionSecurityGroupNamePrefix
  description = var.BastionSecurityGroupDescription
  vpc_id      = aws_vpc.vpc.id
}
resource "aws_security_group_rule" "bastion_ssh_ingress" {
  security_group_id = aws_security_group.bastion.id
  type              = "ingress"
  protocol          = var.BastionSSHIngressProtocol
  from_port         = var.BastionSSHIngressFromPort
  to_port           = var.BastionSSHIngressToPort
  cidr_blocks       = var.BastionSSHIngressCIDR

}
resource "aws_security_group_rule" "bastion_ssh_egress" {
  security_group_id = aws_security_group.bastion.id
  type              = "egress"
  protocol          = var.BastionSSHEgressProtocol
  from_port         = var.BastionSSHEgressFromPort
  to_port           = var.BastionSSHEgressToPort
  cidr_blocks       = var.BastionSSHEgressCIDR

}


# ec2 ingress
resource "aws_security_group" "ec2" {
  # name        = "terraform-remote-ec2"
  name_prefix = var.InstanceSecurityGroupNamePrefix
  description = var.InstanceSecurityGroupDescription
  vpc_id      = aws_vpc.vpc.id
}
resource "aws_security_group_rule" "ec2_allow_ssh" {
  type                     = "ingress"
  protocol                 = var.InstanceSSHIngressProtocol
  from_port                = var.InstanceSSHIngressFromPort
  to_port                  = var.InstanceSSHIngressToPort
  security_group_id        = aws_security_group.ec2.id
  source_security_group_id = aws_security_group.bastion.id
}

resource "aws_security_group_rule" "ec2_ingress_80" {
  security_group_id        = aws_security_group.ec2.id
  type                     = "ingress"
  protocol                 = var.InstanceIngress80Protocol
  from_port                = var.InstanceIngress80FromPort
  to_port                  = var.InstanceIngress80ToPort
  source_security_group_id = aws_security_group.alb.id

}
resource "aws_security_group_rule" "ec2_ingress_443" {
  security_group_id        = aws_security_group.ec2.id
  type                     = "ingress"
  protocol                 = var.InstanceIngress443Protocol
  from_port                = var.InstanceIngress443FromPort
  to_port                  = var.InstanceIngress443ToPort
  source_security_group_id = aws_security_group.alb.id

}

resource "aws_security_group_rule" "ec2_ingress_application_port" {

  security_group_id        = aws_security_group.ec2.id
  type                     = "ingress"
  protocol                 = var.ApplicationProtocol
  from_port                = var.ApplicationPort
  to_port                  = var.ApplicationPort
  source_security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "ec2_egress_80" {
  security_group_id = aws_security_group.ec2.id
  type              = "egress"
  protocol          = var.InstanceEgress80Protocol
  from_port         = var.InstanceEgress80FromPort
  to_port           = var.InstanceEgress80ToPort
  cidr_blocks       = var.InstanceEgress80CIDR
}

resource "aws_security_group_rule" "ec2_egress_443" {
  security_group_id = aws_security_group.ec2.id
  type              = "egress"
  protocol          = var.InstanceEgress443Protocol
  from_port         = var.InstanceEgress443ToPort
  to_port           = var.InstanceEgress443ToPort
  cidr_blocks       = var.InstanceEgress443CIDR
}
# alb ingress

resource "aws_security_group" "alb" {
  # name        = "terraform-remote-alb"
  name_prefix = var.ALBSecurityGroupNamePrefix
  description = var.ALBSecurityGroupDescription
  vpc_id      = aws_vpc.vpc.id
}
resource "aws_security_group_rule" "alb_ingress_80" {
  type              = "ingress"
  protocol          = var.ALBSecurityGroupIngress80Protocol
  from_port         = var.ALBSecurityGroupIngress80FromPort
  to_port           = var.ALBSecurityGroupIngress80ToPort
  security_group_id = aws_security_group.alb.id
  cidr_blocks       = var.ALBSecurityGroupIngress80CIDR
}
# alb ingress 443
resource "aws_security_group_rule" "alb_ingress_443" {
  type              = "ingress"
  protocol          = var.ALBSecurityGroupIngress443Protocol
  from_port         = var.ALBSecurityGroupIngress443FromPort
  to_port           = var.ALBSecurityGroupIngress443ToPort
  security_group_id = aws_security_group.alb.id
  cidr_blocks       = var.ALBSecurityGroupIngress443CIDR
}

resource "aws_security_group_rule" "alb_egress_80" {
  type                     = "egress"
  protocol                 = var.ALBSecurityGroupEgress80Protocol
  from_port                = var.ALBSecurityGroupEgress80FromPort
  to_port                  = var.ALBSecurityGroupEgress80ToPort
  security_group_id        = aws_security_group.alb.id
  source_security_group_id = aws_security_group.ec2.id
}


resource "aws_security_group_rule" "alb_egress_443" {
  type                     = "egress"
  protocol                 = var.ALBSecurityGroupEgress443Protocol
  from_port                = var.ALBSecurityGroupEgress443FromPort
  to_port                  = var.ALBSecurityGroupEgress443ToPort
  security_group_id        = aws_security_group.alb.id
  source_security_group_id = aws_security_group.ec2.id
}

resource "aws_security_group_rule" "alb_egress_app_port" {
  count                    = 1
  type                     = "egress"
  protocol                 = var.ApplicationProtocol
  from_port                = var.ApplicationPort
  to_port                  = var.ApplicationPort
  security_group_id        = aws_security_group.alb.id
  source_security_group_id = aws_security_group.ec2.id
}
