resource "aws_nat_gateway" "main_nat" {
  count         = var.SubnetCount
  allocation_id = aws_eip.nat_eip[count.index].id
  subnet_id     = aws_subnet.public_subnets[count.index].id
  tags          = var.NATGatewayTags
  depends_on = [
    aws_internet_gateway.igw,
    aws_subnet.public_subnets,
    aws_eip.nat_eip
  ]
}
