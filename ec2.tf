
resource "aws_launch_template" "server" {
  image_id      = data.aws_ami.amazon_linux2.id
  instance_type = var.ServerInstanceType
  user_data     = base64encode(data.template_file.user_data_file.rendered)
  key_name      = var.KeyPairName
  network_interfaces {
    security_groups             = [aws_security_group.ec2.id]
    associate_public_ip_address = true
  }
  tag_specifications {
    resource_type = "instance"
    tags          = var.ServerInstanceTagSpec
  }
  tags = var.ServerInstanceTags
  depends_on = [
    aws_security_group.ec2,
    aws_subnet.private_subnets,
    data.aws_ami.amazon_linux2
  ]
}

resource "aws_autoscaling_group" "webserver" {
  name_prefix = var.ServerASGNamePrefix
  launch_template {
    id      = aws_launch_template.server.id
    version = aws_launch_template.server.latest_version
  }
  desired_capacity          = var.ServerASGDesiredCapacity
  min_size                  = var.ServerASGMinSize
  max_size                  = var.ServerASGMaxSize
  vpc_zone_identifier       = aws_subnet.private_subnets.*.id
  health_check_grace_period = var.ServerASGHealthCheckGracePeriod
  health_check_type         = var.ServerASGHealthCheckType
  termination_policies      = var.ServerASGTerminationPolicies
  wait_for_capacity_timeout = var.ServerASGWaitForCapacityTimeout
  target_group_arns         = [aws_lb_target_group.alb_targets.arn]

  tag {
    key                 = var.ServerASGTagKey
    value               = var.ServerASGTagValue
    propagate_at_launch = true
  }
  depends_on = [
    aws_launch_template.server,
    aws_security_group.ec2,
    aws_subnet.private_subnets,
    data.aws_ami.amazon_linux2
  ]
}

data "template_file" "user_data_file" {
  template = file("ec2.tpl")
}
