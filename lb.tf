resource "aws_lb" "alb" {
  name               = var.ALBName
  internal           = var.InternalALB
  load_balancer_type = var.ALBType
  security_groups    = [aws_security_group.alb.id]
  subnets            = aws_subnet.public_subnets.*.id
  depends_on = [
    aws_security_group.alb,
    aws_subnet.public_subnets,
  ]
}
resource "aws_lb_target_group" "alb_targets" {
  name_prefix = var.ALBTargetGroupNamePrefix
  port        = var.ALBTargetGroupPort
  protocol    = var.ALBTargetGroupProtocol
  vpc_id      = aws_vpc.vpc.id
  target_type = var.ALBTargetType
  health_check {
    enabled             = true
    interval            = var.HealthCheckIntervalSeconds
    protocol            = var.HealthCheckProtocol
    timeout             = var.HealthCheckTimeoutSeconds
    healthy_threshold   = var.HealthyThresholdCount
    unhealthy_threshold = var.UnhealthyThresholdCount
    path                = var.HealthCheckPath
    port                = var.HealthCheckPort
  }

  tags = var.ALBTargetGroupTags
  depends_on = [
    aws_vpc.vpc
  ]
}
resource "aws_lb_listener" "alb_http_redirect" {
  load_balancer_arn = aws_lb.alb.arn
  port              = var.ALBListenerPort
  protocol          = var.ALBListenerProtocol
  default_action {
    type             = var.ALBListenerDefaultActionType
    target_group_arn = aws_lb_target_group.alb_targets.arn
  }
  depends_on = [
    aws_lb_target_group.alb_targets
  ]
}
