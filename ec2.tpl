#!/bin/bash

sudo yum update -y

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.1/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
nvm install  node
cat <<EOF >> /home/ec2-user/.bashrc
export NVM_DIR="/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
EOF

node -e "console.log('Running Node.js ' + process.version)"
sudo yum install git -y

git clone https://github.com/zowe/sample-node-api
cd sample-node-api
npm install
npm start

echo "EC2 instance"

 