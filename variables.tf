variable "awsRegion" {
  default = "us-east-1"
}
variable "Environment" {
  default = "dev"
}
variable "ProjectName" {
  default = "Boom-LineZ"
}
# gateway
variable "GatewayName" {
  type = string
}
variable "GatewayDescription" {
  type = string
}
variable "GatewayDefaultResourcePath" {
  type = string
}
# acm certificate
variable "AcmCertDomain" {
  type = string
}
variable "AcmCertStatuses" {
  type = list(string)
}
variable "HostedZoneName" {
  type = string
}
variable "Route53RecordName" {
  type = string
}
variable "Route53RecordType" {
  type = string
}


# vpc
variable "VPCCIDR" {
  type = string
}
variable "EnableDNSSupport" {
  type = bool
}
variable "EnableDNSHostnames" {
  type = bool
}
variable "InstanceTenancy" {
  type = string
}

variable "VPCTags" {
  type = map(string)
}


# igw
variable "IGWTags" {
  type = map(string)
}
# subnets
variable "PublicSubnetMapPublicIpOnLaunch" {
  type = bool
}
variable "PublicSubnetAssignIpv6AddressOnCreation" {
  type = bool
}

variable "PublicSubnetTags" {
  type = map(string)
}
variable "PrivateSubnetTags" {
  type = map(string)
}
# nat
variable "NATGatewayTags" {
  type = map(string)
}
# eip



# alb

variable "ALBName" {
  type = string
}
variable "InternalALB" {
  type = bool
}
variable "ALBType" {
  type = string
}


variable "ALBTargetGroupNamePrefix" {
  type = string
}
variable "ALBTargetGroupProtocol" {
  type = string
}
variable "ALBTargetGroupPort" {
  type = number
}
variable "ALBTargetType" {
  type = string
}



variable "EnableHealthCheck" {
  type = bool
}
variable "HealthCheckProtocol" {
  type = string
}
variable "HealthCheckPort" {
  type = number
}
variable "HealthCheckPath" {
  type = string
}
variable "HealthCheckIntervalSeconds" {
  type = number
}
variable "HealthCheckTimeoutSeconds" {
  type = number
}
variable "HealthyThresholdCount" {
  type = number
}
variable "UnhealthyThresholdCount" {
  type = number
}
variable "ALBTargetGroupTags" {
  type = map(string)
}

variable "ALBListenerPort" {
  type = number
}
variable "ALBListenerProtocol" {
  type = string
}


variable "ALBListenerDefaultActionType" {
  type = string
}


# route tables
variable "PublicRouteTableTags" {
  type = map(string)
}
variable "PrivateRouteTableTags" {
  type = map(string)
}
# api gateway
variable "StageName" {
  type = string
}
variable "QuotaLimit" {
  type = number
}
variable "QuotaOffset" {
  type = number

}
variable "QuotaPeriod" {
  type = string
}
variable "BurstLimit" {
  type = number
}
variable "RateLimit" {
  type = number
}

variable "APIKeyCount" {
  type = number
}
variable "APIKeyNamePrefix" {
  type = string
}
variable "APIKeyDescription" {
  type = string
}

variable "UsagePlanName" {
  type = string
}
variable "UsagePlanDescription" {
  type = string
}




# Ec2 Server instances
variable "ServerInstanceType" {
  type = string
}
variable "ServerInstanceTagSpec" {
  type = map(string)
}
variable "ServerInstanceTags" {
  type = map(string)
}
# Ec2 server asg
variable "ServerASGNamePrefix" {
  type = string
}

variable "ServerASGDesiredCapacity" {
  type = number
}
variable "ServerASGMaxSize" {
  type = number
}
variable "ServerASGMinSize" {
  type = number
}
variable "ServerASGTagKey" {
  type = string
}
variable "ServerASGTagValue" {
  type = string
}
variable "ServerASGHealthCheckGracePeriod" {
  type = number
}
variable "ServerASGHealthCheckType" {
  type = string
}
variable "ServerASGTerminationPolicies" {
  type = list(string)
}
variable "ServerASGWaitForCapacityTimeout" {
  type = number
}
# Bastion instance
variable "BastionInstanceType" {
  type = string
}
variable "BastionInstanceTags" {
  type = map(string)
}


# sg
# variables for security groups 
# bastion
variable "BastionSecurityGroupNamePrefix" {
  type = string
}
variable "BastionSecurityGroupDescription" {
  type = string
}
variable "BastionSSHIngressCIDR" {
  type = list(string)
}
variable "BastionSSHIngressFromPort" {
  type = string
}
variable "BastionSSHIngressToPort" {
  type = string
}
variable "BastionSSHIngressProtocol" {
  type = string
}
variable "BastionSSHEgressCIDR" {
  type = list(string)
}
variable "BastionSSHEgressFromPort" {
  type = number
}
variable "BastionSSHEgressToPort" {
  type = number
}
variable "BastionSSHEgressProtocol" {
  type = string
}

# ec2 servers
variable "InstanceSecurityGroupNamePrefix" {
  type = string
}
variable "InstanceSecurityGroupDescription" {
  type = string
}

variable "InstanceSSHIngressFromPort" {
  type = number
}
variable "InstanceSSHIngressToPort" {
  type = number
}
variable "InstanceSSHIngressProtocol" {
  type = string
}

variable "InstanceIngress80Protocol" {
  type = string
}
variable "InstanceIngress80FromPort" {
  type = number
}
variable "InstanceIngress80ToPort" {
  type = number
}


variable "InstanceIngress443Protocol" {
  type = string
}
variable "InstanceIngress443FromPort" {
  type = number
}
variable "InstanceIngress443ToPort" {
  type = number
}


variable "InstanceEgress80Protocol" {
  type = string
}
variable "InstanceEgress80FromPort" {
  type = number
}
variable "InstanceEgress80ToPort" {
  type = number
}
variable "InstanceEgress80CIDR" {
  type = list(string)
}



variable "InstanceEgress443Protocol" {
  type = string
}
variable "InstanceEgress443FromPort" {
  type = number
}
variable "InstanceEgress443ToPort" {
  type = number
}
variable "InstanceEgress443CIDR" {
  type = list(string)
}


# alb
variable "ALBSecurityGroupNamePrefix" {
  type = string
}
variable "ALBSecurityGroupDescription" {
  type = string
}
variable "ALBSecurityGroupIngress80CIDR" {
  type = list(string)
}
variable "ALBSecurityGroupIngress80Protocol" {
  type = string
}
variable "ALBSecurityGroupIngress80FromPort" {
  type = number
}
variable "ALBSecurityGroupIngress80ToPort" {
  type = number
}




variable "ALBSecurityGroupIngress443CIDR" {
  type = list(string)
}
variable "ALBSecurityGroupIngress443Protocol" {
  type = string
}
variable "ALBSecurityGroupIngress443FromPort" {
  type = number
}
variable "ALBSecurityGroupIngress443ToPort" {
  type = number
}




variable "ALBSecurityGroupEgress443Protocol" {
  type = string
}
variable "ALBSecurityGroupEgress443FromPort" {
  type = number
}
variable "ALBSecurityGroupEgress443ToPort" {
  type = number
}





variable "ALBSecurityGroupEgress80Protocol" {
  type = string
}
variable "ALBSecurityGroupEgress80FromPort" {
  type = number
}
variable "ALBSecurityGroupEgress80ToPort" {
  type = number
}





variable "ApplicationPort" {
  type = number
}

variable "ApplicationProtocol" {
  type = string
}




variable "SubnetCount" {
  default = 2
}

variable "PublicRouteTableCIDR" {
  type = string
}

variable "PrivateRouteTableCIDR" {
  type = string
}

variable "InstanceCount" {
  default = 2
}

variable "GenerateNewKeyPair" {
  type = bool
}
variable "KeyPairName" {
  type = string
}
variable "EnableBastion" {
  default = true
}
variable "BastionCount" {
  default = 1
}







