resource "aws_vpc" "vpc" {
  cidr_block           = var.VPCCIDR
  enable_dns_hostnames = var.EnableDNSHostnames
  enable_dns_support   = var.EnableDNSSupport
  instance_tenancy     = var.InstanceTenancy
  tags                 = var.VPCTags
}
